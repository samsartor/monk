# `monk` Change Log
-----

## 0.2.0 - 2020-16-29

### Added

* Added a [LICENSE](monk-cli/LICENSE) (Apache 2) to `monk-cli`
* Added a [LICENSE](monkd/LICENSE) (AGPLv3) to `monkd`
* Added descriptions to both `Cargo.toml`s
* Added a [CHANGELOG.md](CHANGELOG.md)